.. _sec:vision:

Vision
======

We believe that a participatory protocol tied directly to the
communities practicing skills and defining ideas will diminish the gap
between credentials that can be credibly assessed and issued and the
wide variety of skills and abilities that people are capable of.

Tying economic incentives to this social process and ontology, such that
they are both visible to everybody and aligned amongst all those
participating, allows for fathom-credentials to be trustworthy and
transparent.

Distributing the work required to communities allows the system to scale
and be accessible to anyone, no matter their previous records,
achievements or socio-economic circumstances.

We believe that through these traits fathom enables a world where people
are free to shape their own experiences, communicate them to others, and
organize to achive shared ambitions.
